# Contributing to radicons

_👉 If you only want to **suggest** a new icon,
[you can do that here](https://gitlab.com/radicons/radicons/-/issues/new?issue%5Btitle%5D=New%20icon%3A%20&issuable_template=New%20Icon)._

If you want to **directly contribute** a new icon, or contribute to the
scaffolding code that does things like auto-generates PNG versions from SVG
versions, then read on!

## New icons

At this time the only mechanism we have for folks to submit an icon to the
collection is by
[opening a Merge Request](https://gitlab.com/radicons/radicons/-/merge_requests/new).
In the future we hope to have a non-GitLab form that can be submitted from a
website to contribute a new icon.

If you're not familiar with git/GitLab and the idea of opening a Merge Request
is overwhelming, head over to
[suggest a new icon](https://gitlab.com/radicons/radicons/-/issues/new?issue%5Btitle%5D=New%20icon%3A%20&issuable_template=New%20Icon)
and in the description provide a link to the file.

**[Our guidelines for new icons.](https://gitlab.com/radicons/radicons/-/blob/main/docs/icon-guidelines.md)**

## Improving the code

This collection is focused on the icons, but there is a little bit of code,
written in [Node](https://nodejs.org/en/), to help make it run smoothly, like:

- Auto-generating PNG versions of SVG files
- Auto-generating the thumbnails for
  [this collection](https://gitlab.com/radicons/radicons) and
  [other icons](https://gitlab.com/radicons/radicons/-/blob/main/others/README.md)
- Running some GitLab-based actions and continuous integration
- In the future, presenting it all on an easy-to-navigate website

If you'd like to help us out with these things, we'd really appreciate it!

**Note:** radicons has a
[code of conduct](https://gitlab.com/radicons/radicons/-/blob/main/CODE_OF_CONDUCT.md).
Please follow it in all your interactions with the project.

### Technical guidelines for the icons

For consistency and accessibility, we have some
[technical guidelines for the icons and associated files](https://gitlab.com/radicons/radicons/-/blob/main/docs/technical-guidelines.md).

### How to submit a Merge Request

0. If you're more familiar with GitHub, a Merge Request is like a Pull Request
   😼

1. Search our repository for open or closed
   [Merge Requests](https://gitlab.com/radicons/radicons/-/merge_requests) that
   relate to your submission. You don't want to duplicate effort.

1. [Fork the project.](https://gitlab.com/radicons/radicons/-/forks/new)

1. Create your feature branch:

   ```sh
   git checkout -b feat/amazing_feature
   ```

1. Do your work.

1. Commit your changes:

   ```sh
   git commit -m 'feat: add amazing_feature'
   ```

1. Push to the branch:

   ```sh
   git push origin feat/amazing_feature
   ```

1. [Open a Merge Request](https://gitlab.com/radicons/radicons/-/merge_requests/new)
   and wait for feedback.

### 🎉 **Thank you for your contribution!**
