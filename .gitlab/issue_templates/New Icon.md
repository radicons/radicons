<!-- THANK YOU for your suggestion! Please replace these comments below with more information about your suggestion. And remember to give your suggestion a title, at the top of this page. -->

- [ ] I have read the
      [guidelines for new icons](https://gitlab.com/radicons/radicons/-/blob/main/docs/icon-guidelines.md)

## Request or submission?

- [ ] I've already created a new icon in SVG format, and want to submit it to
      this collection.
- [ ] There's an existing image that I think could be adapted or used as the
      basis for a new icon.
- [ ] I have an idea for a new icon, but there's no existing image.

## Description

<!-- Describe what it looks like and -- especially if it's not immediately clear -- what it means. -->

## Link to existing image(s)

<!-- If you have an icon already created, include a link to it here. If you want to suggest an image to be adapted into an icon, include a link to the highest-resolution version of this image you can find. -->

## Examples of this image/iconography being used in the wild

<!-- If you know it's being used somewhere in some form, including one or more links so we can take a look! -->

## Any information about the history of this image

<!-- Did this image just emerge? Was it used historically? Share anything you know about its history. -->

## Additional links or references

<!-- If there are Wikipedia entries, blogs, videos, etc. about it, please include them here. -->

<!-- Please leave the next line in place. -->

/label ~newicon
