<!-- THANK YOU for your suggestion!

  NOTE: If you're suggesting a new icon, please use this link:

  https://gitlab.com/radicons/radicons/-/issues/new?issue[title]=New%20icon%3A%20&issuable_template=New%20Icon

  Otherwise, please replace these comments below with more information about your suggestion. And remember to give your feature a title, at the top of this page. -->

## Describe the feature

<!-- A clear and concise description of your feature request. Please note if your request is related to a problem. -->

## Describe preferred solution

<!-- A clear and concise description of what you want to happen. -->

## Describe alternatives

<!-- A clear and concise description of any alternative solutions or features you've considered. -->

## Related code

<!-- If you are able to illustrate this request with an example, please provide it here. -->

## Additional context

<!-- List any other information that is relevant to your issue. Use cases, screenshots, related issues, stack traces, links to relevant information, environment information if applicable, etc. -->

**If the feature request is approved, would you be willing to submit a Merge
Request?** _(Help can be provided if you need assistance submitting an MR.)_

- [ ] Yes
- [ ] No

<!-- Please leave the next line in place. -->

/label ~newfeat
