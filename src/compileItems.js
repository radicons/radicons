'use strict'

const fs = require('fs')
const path = require('path')
const fg = require('fast-glob')

const onlyUnique = require('./onlyUnique.js')

const imageFormats = ['svg', 'png', 'jpg', 'jpeg', 'gif']
const textFormats = ['txt', 'text', 'yml', 'yaml']
const excludeFormats = ['md', 'html', 'js']

// Compile list of unique items based on file names, within a given directory
module.exports = function compileItems(directory) {
  const list = {
    dir: path.join('.', directory),
    items: [],
  }

  // Get each item's name
  let names = []
  fg.sync(path.join(list.dir, `*.!(${excludeFormats})`)).forEach(function (
    file
  ) {
    let name = path.basename(file, path.extname(file))
    names.push(name)
  })

  // Set properties for each item
  list.items = names.filter(onlyUnique).map((name) => {
    // retrieve the file paths
    let imageFile = fg.sync(path.join(list.dir, `${name}.{${imageFormats}}`))[0]
    let dataFile = fg.sync(path.join(list.dir, `${name}.{${textFormats}}`))[0]

    // remove the directory now that we have the right files
    const listReplace = new RegExp(`${list.dir}/`)
    let imageURL = `${imageFile}`.replace(listReplace, '')
    let dataURL = `${dataFile}`.replace(listReplace, '')

    // set the format based on the extension
    let format = path.extname(imageURL).replace(/^\./g, '').toUpperCase()

    let properties = {
      name: name,
      dir: list.dir,
      format: format,
      image: imageURL,
      data: dataURL,
    }

    return properties
  })

  // Remove items that have no images. Because we stringify the image URL above,
  // undefined images actually match _the string_ "undefined".
  list.items = list.items.filter(function (element) {
    return element.image !== 'undefined'
  })

  return list.items
}
