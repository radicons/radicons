'use strict'

const retrieveMetadata = require('./retrieveMetadata.js')

// Return grid markup for supplied list of items
module.exports = function prepareMarkup (items, cols, type, done) {
  const borders = cols + 1 // e.g. 5 columns need 6 borders
  let markup, header, rule, wrapOpen, wrapClose, rowEnd, footer

  if (type === 'md') {
    header = '|   '.repeat(borders).slice(0, -3) + '\n'
    rule = '|---'.repeat(borders).slice(0, -3) + '\n'
    wrapOpen = '|'
    wrapClose = ''
    rowEnd = '|'
    footer = '|'
  } else if (type === 'html') {
    header = '<div>'
    rule = ''
    wrapOpen = '<div>'
    wrapClose = '</div>'
    rowEnd = ''
    footer = '</div>'
  } else {
    return done(Error('Unknown type for prepareMarkup()'))
  }

  markup = header + rule

  // Add metadata to the item
  items.forEach(function (item) {
    retrieveMetadata(item)
  })

  // Sort alphabetically
  items.sort((a, b) => a.title.localeCompare(b.title))

  // Provide the markup
  let i = 0
  items.forEach(function (item) {
    // Define required elements
    const image = `<img src="${item.image}" width="100" />`
    const license = `<br /><strong>License:</strong> ${item.license}`

    // Define optional elements
    let title = ''
    let description = ''
    let artists = ''
    let sources = ''
    let references = ''
    let linkLabel = ''

    if (item.title !== undefined && item.title !== null) {
      title = `<h3>${item.title}</h3><strong>Format:</strong> <a href="${item.image}" title="Download this image">${item.format}</a>`
    }

    if (item.description !== undefined && item.description !== null) {
      description = `<br /><strong>Description:</strong> ${item.description}`
    }

    if (item.artistsList !== undefined && item.artistsList !== null) {
      artists = `<br /><strong>Artist(s):</strong> ${item.artistsList}`
    }

    if (item.sourceLinksList !== undefined && item.sourceLinksList !== null) {
      sources = `${item.sourceLinksList} `
    }

    if (item.refLinksList !== undefined && item.refLinksList !== null) {
      references = `${item.refLinksList}`
    }

    if (sources || references) {
      linkLabel = '<br /><strong>Links:</strong> '
    }

    markup += `${wrapOpen}${image}${title}${description}${artists}${license}${linkLabel}${sources}${references}${wrapClose}`
    i = i + 1
    if ((i + 1) % cols === 0) {
      markup += `${rowEnd}\n`
    }
  })

  markup += footer

  return done(null, markup)
}
