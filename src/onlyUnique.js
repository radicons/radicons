'use strict'

// Filter an array to only return unique values
module.exports = function onlyUnique(value, index, self) {
  return self.indexOf(value) === index
}
