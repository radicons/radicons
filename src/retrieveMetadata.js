'use strict'

const fs = require('fs')
const path = require('path')
const yaml = require('js-yaml')

// Retrieve metadata on an item from its accompanying YAML file.
module.exports = function retrieveMetadata (item) {
  const filePath = path.join(item.dir, item.data)
  const data = yaml.loadAll(fs.readFileSync(filePath, { encoding: 'utf-8' }))[0]

  // Provide a concatenated list of artists
  data.artistsList = data.artists.join(', ')

  // Provide a concatenated and formatted list of image sources
  if (data.imageURLs) {
    data.sourceLinks = data.imageURLs.map(item => {
      if (item !== null) {
        const link = `[<a href="${item}" title="View this image's source">source</a>]`
        return link
      }
    })
    data.sourceLinksList = data.sourceLinks.join(' ')
  }

  // Provide a concatenated and formatted list of reference links
  if (data.referenceURLs) {
    data.refLinks = data.referenceURLs.map(item => {
      if (item !== null) {
        const link = `[<a href="${item}" title="More information about this image">reference</a>]`
        return link
      }
    })
    data.refLinksList = data.refLinks.join(' ')
  }

  // Merge all of the properties from data into the item object, overwriting
  // if necessary
  Object.assign(item, data)

  return item
}
