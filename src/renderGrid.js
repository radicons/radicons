'use strict'

const compileItems = require('./compileItems.js')
const prepareMarkup = require('./prepareMarkup.js')

module.exports = function renderGrid(path) {
  const items = compileItems(path)

  const grid = prepareMarkup(items, 5, 'md', function (err, markup) {
    if (err) console.log('error', err.message, err.stack)
    else return markup
  })

  console.log(grid)
}
