# Radical Icons

Radical activist/protest/anarchist icons, SVG and PNG, free for use in all
projects. <http://radicons.art>

👉🏼
[Suggest a new icon](https://gitlab.com/radicons/radicons/-/issues/new?issue[title]=New%20icon%3A%20&issuable_template=New%20Icon)
&bullet;
[Suggest a new feature](https://gitlab.com/radicons/radicons/-/issues/new?issue[title]=New%20feature%3A%20&issuable_template=New%20Feature)
&bullet;
[Report a bug](https://gitlab.com/radicons/radicons/-/issues/new?issue[title]=Bug%3A%20&issuable_template=Bug)
&bullet;
[Ask a question](https://gitlab.com/radicons/radicons/-/issues/new?issue[title]=Question%3A%20&issuable_template=Support)
👈🏿

**[Code of conduct](CODE_OF_CONDUCT.md)** &bullet;
**[How to contribute](CONTRIBUTING.md)**

---

<dl><dt>"The role of the artist is to make the revolution irresistible."</dt><dd>&#8212;Toni Cade Bambara</dd><dl>

---

## The collection

_Icons for this collection are underway. For now, check out images from other
sources and other resources, below._

## Other images

In the [others](others/README.md) directory, you'll find images sourced from
other locations. These are in various image formats and resolutions, and are
released in a variety of free-to-reuse licenses. Consult the text document
alongside each image for more information.

# Resources

Additional sources of radical icons, licenses/usage options noted:

- [LaserBloc](https://drive.google.com/drive/u/0/folders/11Or4FlJJKIKlRkx3-WMh8DN-3UCLSfvC)
  (Google Drive folder): Not strictly icons, but could be used as such. See
  [readme](https://docs.google.com/document/d/16psMSpOlmywLIvImf_Z5Ui7FDUS_0Nhe0lTW0mX3CUg/edit)
  and
  [license](https://docs.google.com/document/d/10lOEfOZisMR9B3_gW7jFmYizkxa0ePdFvJfhaRk2o5U/edit)
  (Google Docs). Other designs at [acab.city](https://acab.city) and
  [redistribute.capital](https://redistribute.capital) (Google Drive and Google
  Form).
- [Open Clipart](https://openclipart.org/) has lots of iconography and line
  drawings, all public domain, in both SVG and PNG formats.
- [Noun Project: activist](https://thenounproject.com/term/activist/): Creative
  Commons–licensed.
- [Noun Project: radical](https://thenounproject.com/term/radical/): Creative
  Commons–licensed.
- [flaticon: activist](https://www.flaticon.com/free-icons/activist):
  attribution required. Includes ads for paid stock icons.
- [iconfinder: activist](https://www.iconfinder.com/search?q=activist):
  primarily paid.
- [iconfinder: radical](https://www.iconfinder.com/search?q=radical): primarily
  paid.
- Suggest more sources here by creating a
  [new issue](https://gitlab.com/radicons/radicons/-/issues/new) or
  [new merge request](https://gitlab.com/radicons/radicons/-/merge_requests/new).

# Inspiration

[iconmonstr](https://iconmonstr.com/), which has simple, beautiful, free icons.

# License

Generally we believe software
[should be released under the AGPL](https://www.gnu.org/licenses/why-affero-gpl.html)
and artwork should be released under the
[Free Art License](https://artlibre.org/licence/lal/en/) or the
[Creative Commons Attribution-ShareAlike License](https://creativecommons.org/licenses/by-sa/4.0/).

However, in order to ensure maximum free usage without burdening anyone with
including copyright information on your agitprop, everything here is released as
[CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) -- that
is, it is released into the public domain without copyright. This means you are
free to do what you like with these images and you are not required to provide
credit. See [the license](LICENSE).

<a rel="license" href="http://creativecommons.org/publicdomain/mark/1.0/"><img src="http://i.creativecommons.org/p/mark/1.0/88x31.png"
     style="border-style: none;" alt="Public Domain Mark" /></a>

**If you would _like_ to show your support, however, feel free to credit us as
[radicons.art](http://radicons.art)**

As noted above, images located in the [others](others) directory were released
under a variety of free-to-reuse licenses, and are not covered under this
collection's general license. See [others/LICENSE](others/LICENSE) for details.
