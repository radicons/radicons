# Technical guidelines for icons

These guidelines are meant to ensure consistency and accessibility. If they're
not living up to that, file a Merge Request and let's change them!

## Structure

- each icon in its own directory
- each icon including a text file in YAML with description, history of symbol,
  links, resources: [see the text file template](icon-text-template.yml)
- a planned feature is supporting translations, either within each file or by
  using similarly-named files (`symbol_en.yml`, `symbol_es.yml`, etc.)

## SVG

For each icon:

- minified with [nano](https://vecta.io/nano),
  [svgo](https://github.com/svg/svgo) or something similar to reduce syntax as
  much as possible (without clobbering the following requirements)
- when possible things like outlines should be converted to paths, not relying
  on styling
- 500x500 viewport; if an icon cannot reasonably be made square even with
  margins, then: max x-width 500px, unless at that scale y-height > 500px, in
  which case y-height 500px
- avoid using a wrapping `transform` attribute for something straightforward
  like scaling; make the paths actually fit the viewport
- svg wrapper includes `xmlns` and `role="img"`
- includes `title`, `desc` and `aria-labelledby`
- includes CSS classes on each component
- include `role="presentation"` on each component
- if using `text`, ensure that it is pleasing without relying on CSS styling,
  and only use web-safe fonts or commit to base-64-inlining the font file (in
  which case it must be free of copyright!)
- if using a path to represent text, that component should include an
  `aria-label` reflecting that
- includes basic CSS styling

### SVG example including all of the above

```html
<svg
  xmlns="http://www.w3.org/2000/svg"
  width="500"
  height="500"
  role="img"
  aria-labelledby="symbol_title symbol_desc"
>
  <title id="symbol_title">Symbol name</title>
  <desc id="symbol_desc">Short description of symbol</desc>
  <style type="text/css">
    <![CDATA[
    .disc {
    fill: #cde4d9;
    }
    .word {
    fill: #000;
    }
    .bar {
    fill: #000;
    }
    ]]>
  </style>
  <rect
    class="disc"
    role="presentation"
    width="500"
    height="500"
    x="0"
    y="0"
    rx="500"
    ry="500"
  />
  <g class="word" role="presentation">
    <path d="[...]" aria-label="word" />
  </g>
  <path class="bar" role="presentation" d="[...]" />
</svg>
```

### SVG resources

- https://css-tricks.com/accessible-svgs/#2-inline-svg
- https://www.sitepoint.com/tips-accessible-svg/
- https://www.deque.com/blog/creating-accessible-svgs/ (note testing results are
  from 2019)
- https://www.w3.org/TR/wai-aria/#img
- https://www.w3.org/TR/wai-aria/#namecalculation

## PNG

- minified **LOSSLESSLY**\* with
  [pngcrush](https://pmt.sourceforge.io/pngcrush/),
  [optipng](http://optipng.sourceforge.net/),
  [tinify](https://github.com/tinify),
  [pngout](http://www.advsys.net/ken/utils.htm), etc.
- same dimensions as corresponding SVG viewport (see above) of icon
- negative space should be transparent

Ideally this will all be done in a build script automatically.

\* We want to provide the highest-res versions without things like image
interpolation, since in some cases these will be used in print media where file
size is not an issue in the way it is for the web. If someone wants to use PNG
in a website, they can always choose to further minify it to an acceptable
degree of quality for them.
