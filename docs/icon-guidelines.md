# Guidelines for new icons

If you'd like to
[suggest a new icon](https://gitlab.com/radicons/radicons/-/issues/new?issue[title]=New%20icon%3A%20&issuable_template=New%20Icon),
that's great! Here are a few guidelines we have for what we're including:

0. Iconography should follow our
   [code of conduct](https://gitlab.com/radicons/radicons/-/blob/main/CODE_OF_CONDUCT.md).

1. The focus is on radical politics, activism, protesting, anarchism, socialism,
   and other things that fit in the framework of
   [lower leftism](https://birdsbeforethestorm.net/2016/10/lower-leftism-expanding-upon-the-political-map/).

1. We are generous when it comes to sectarian differences in political beliefs,
   as long as they're part of a general radical history. (Except
   "anarcho-capitalism" -- if that's something you need, the free market will
   provide it for you, right?)

1. Any number of generic images or ideas could theoretically be useful to, say,
   a great radical sticker. But there are [lots](https://iconmonstr.com/) of
   [sources](https://www.iconfinder.com/) for
   [generic icons](https://thenounproject.com/) (not to mention
   [free-to-use images in general](https://search.creativecommons.org/), so to
   keep this collection manageable we're not going to try to include everything
   in the world here.

1. In particular, lots of weapons (grenades, tanks, bombs, etc.) could be
   included here as a means to create their negative (e.g. a slash through a
   tank to represent "peace"), but we're not going to include them unless
   they're _already a part of that context_ (e.g. an icon that includes that
   slash). There are a few exceptions for things like molotov cocktails and
   slingshots that are particularly associated with radical activism.

1. While we love personal creativity, the idea for these icons are to provide
   building blocks. Your vision board or custom flag representing all your
   beliefs is awesome, but not appropriate for a collection of icons.

1. This is a volunteer project. If your request isn't being addressed, it's not
   because of political differences or ignorance of how much we need a
   particular icon. Please be patient.

**These are not rules, but guidelines.**

Separately, there are some [technical guidelines](technical-guidelines.md) in
order to maximize consistency and accessibility.
