#!/usr/bin/env node
'use strict'

const renderGrid = require('./src/renderGrid.js')
const retrieveMetadata = require('./src/retrieveMetadata.js')

module.exports.updateGrid = function updateGrid() {
  renderGrid('others')
}

module.exports.meta = function meta() {
  // temporary hardcoding of item
  const item = others.items[0]

  // get metadata
  const meta = retrieveMetadata(item)
  return meta
}
